package db.nav.utils;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;

public class ConfigFileReader {
	 
	 private Properties properties;
	 private final String propertyFilePath= "C:\\Users\\Girmiti\\BBPOS-WP\\bbpos\\src\\test\\resources\\extent.properties";
	 private static ConfigFileReader ConfigFileReader;
	 
	 
	 public ConfigFileReader(){
	 BufferedReader reader;
	 try {
	 reader = new BufferedReader(new FileReader(propertyFilePath));
	 properties = new Properties();
	 try {
	 properties.load(reader);
	 reader.close();
	 } catch (IOException e) {
	 e.printStackTrace();
	 }
	 } catch (FileNotFoundException e) {
	 e.printStackTrace();
	 throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
	 } 
	 }	
	 public String getDriverPath(){
			String driverPath = properties.getProperty("driverPath");
			if(driverPath!= null) return driverPath;
			else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");		
		}
		
		public long getImplicitlyWait() {		
			String implicitlyWait = properties.getProperty("implicitlyWait");
			if(implicitlyWait != null) return Long.parseLong(implicitlyWait);
			else throw new RuntimeException("implicitlyWait not specified in the Configuration.properties file.");		
		}
		
		public String getApplicationUrl() {
			String url = properties.getProperty("url");
			if(url != null) return url;
			else throw new RuntimeException("url not specified in the Configuration.properties file.");
		}
		
		 public String getreportConfigPath(){
				String reportConfigPath = properties.getProperty("reportConfigPath");
				if(reportConfigPath!= null) return reportConfigPath;
				else throw new RuntimeException("reportConfigPath not specified in the Configuration.properties file.");		
			}
		 
		 public static ConfigFileReader getInstance( ) {
		     
			if(ConfigFileReader == null) {
		    	 ConfigFileReader = new ConfigFileReader();
		     }
		        return ConfigFileReader;
		    }
		
		


}