package db.nav.test.runners;

import static db.nav.core.constants.Features.DB_APP;
import static  db.nav.core.constants.Steps.DB_STEPS_DEFINITIONS;


import java.io.File;

import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(
        features = {DB_APP},
        //format = {"json:target/cucumber.json", "html:target/site/cucumber-pretty"},
        glue = {DB_STEPS_DEFINITIONS},
        //~ will skip the features with that specific tag
        //tags ={"@~ignore"},
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:report/byod-purchase_tip-partial-refund-report.html"},
        monochrome = true
        
        
)
 
public class TestRunner{
	
	//BYOD_COMMON_GUI,BYOD_BLE,BYOD_Debit_Credit,Full_Refund_Debit_Credit,PARTIAL_REFUND_DEBIT_CREDIT,BYOD_Purchase_with_Tip,Full_Refund_Purchase_with_Tip,PARTIAL_REFUND_PURCHASE_WITH_TIP
	/*
	 * @Before public void beforeSuite() { ExtentReports report; // report. //report
	 * = new ExtentReports(System.getProperty("user.dir") +
	 * "/reports/Report.html",true); //report.addSystemInfo(�Host Name�,
	 * �XYZ�).addSystemInfo(�Environment�, �Dev�).addSystemInfo(�User Name�, �ABC�);
	 * //report.loadConfig(new File(System.getProperty("u ser.dir") +
	 * "\\extent-config.xml")); }
	 */
     //writes in the report
    
    public static void writeExtentReport() {
        try {
            Reporter.loadXMLConfig(new File(db.nav.utils.ConfigFileReader.getInstance().getreportConfigPath()));
           // Reporter.loadXMLConfig(bbpos.utils.ConfigFileReader.getInstance().getreportConfigPath());
	        //Reporter.loadXMLConfig("/config/extent-Config.xml");
            Reporter.setSystemInfo("user", System.getProperty("user.name"));
            Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
            Reporter.setSystemInfo("Machine", "Windows 10" + "64 Bit");
            Reporter.setSystemInfo("Selenium", "3.141.59");
            Reporter.setSystemInfo("Maven", "3.7.0");
            Reporter.setSystemInfo("Java Version", "11");
            Reporter.setTestRunnerOutput("BBPOS Terminal Output");
            
        }catch(Exception e){
            System.out.println("Error: " + e);
        }
          
        


    }

  
}

