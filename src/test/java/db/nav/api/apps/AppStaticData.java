package db.nav.api.apps;

public interface AppStaticData {
	String APP_PACKAGE_ID = "de.hafas.android.db"; 
	String APP_LAUNCHER_ACTIVITY_ID = "de.bahn.dbtickets.ui.DBNavLauncherActivity";
}
