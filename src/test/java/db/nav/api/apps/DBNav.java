package db.nav.api.apps;

import static db.nav.api.apps.AppStaticData.APP_LAUNCHER_ACTIVITY_ID;
import static db.nav.api.apps.AppStaticData.APP_PACKAGE_ID;
import static db.nav.core.constants.Constants.BBPOS_APK;

import db.nav.api.android.Android;
import db.nav.api.apps.interfaces.Application;


public class DBNav implements Application {

	

	@Override
	public Object open(String deviceID) {
		try {
			Android.adb.startApp(APP_PACKAGE_ID, APP_LAUNCHER_ACTIVITY_ID, deviceID);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void forceStop(String deviceID) {
		try {
			Android.adb.stopApp(APP_LAUNCHER_ACTIVITY_ID, APP_LAUNCHER_ACTIVITY_ID, deviceID);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getPackageID() {
		return APP_PACKAGE_ID;
	}

	@Override
	public String getLuncherActivityID() {
		return APP_LAUNCHER_ACTIVITY_ID;
	}

	@Override
	public void clearAppData(String deviceID) {
		Android.adb.clearAppData(deviceID, getPackageID());
	}

	@Override
	public String getApkPath() {
		return BBPOS_APK;
	}

	

}
