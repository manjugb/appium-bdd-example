package db.nav.api.apps.testSteps;

import static db.nav.core.Constants.SCREEN_SHOTS_FOLDER;

import java.util.ArrayList;
import java.util.Properties;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.mongodb.DB;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import db.nav.api.android.Android;
import db.nav.api.apps.db.DBActivity;
import db.nav.core.ADB;
import db.nav.core.CapabilitiesDevice;
import db.nav.core.FreeDevicefinder;
import db.nav.core.MyLogger;
import db.nav.core.UIObjectWrapper;
import db.nav.core.manager.DriverManager;
import db.nav.model.Device;
import db.nav.test.runners.TestStarter;

public class DBSteps {
	private String deviceID = "";

	private Device device;
	private String filename;
	public DB db;
	private FreeDevicefinder freeDevicefinder;
	private DBActivity dbctivity;
	UIObjectWrapper cstenv,devenv,qatenv,proenv;
	static ExtentTest test;
	private static String propertiy_filenames;
	private static Properties properties1;

	

	public ADB adb = new ADB();

	// private Veras Aproved Messageifone verifone;

	public Device getFreeDevice() {
		freeDevicefinder = FreeDevicefinder.getInstance();
		return freeDevicefinder.findReadyDevice();
//		return freeDevicefinder.findFreeDevice();
	}
	
	
	
	


	@Given("^DB App Launched$")
	public void verifone_app_is_started() {
		// device = findFreeDevice();
		device = getFreeDevice();
		// apkName=BBPOS_APK;
		if (device != null) {
			try {
				dbctivity = new DBActivity(device.getDeviceID());

				// adb.installApp("emulator-5554", BBPOS_APK);
				// adb.installApp("emulator-5556", DBNAV_APK);
				// dm.createAndroidDriver();
				// adb.installApp(Android.adb.getDeviceID(), apkName);

				// assertTrue(adb.getDeviceID().toString().contentEquals(deviceId));
				MyLogger.logger.info(adb.getAndroid_plattform_path());
				MyLogger.logger.info(adb.getConnectedDeviceIds());

				CapabilitiesDevice capas = TestStarter.getCapas(device, Android.apps.dbnav.getPackageID(),
						Android.apps.dbnav.getLuncherActivityID());

				deviceID = capas.getDEVICEID();

				DriverManager.createAndroidDriver(capas, device.getPort());

				Android.apps.dbnav.open(Android.adb.getDeviceID());

				MyLogger.logger.info("starting test of " + "  " + Android.apps.dbnav.getPackageID() + " on "
						+ capas.getDEVICEID() + "  plattform version " + capas.getPLATFORM_VERSION() + " "
						+ Android.apps.dbnav.getLuncherActivityID());

				adb.getConnectedDeviceIds();
				ArrayList<String> devices = adb.getConnectedDeviceIds();
				// System.out.println(devices);

				MyLogger.logger
						.debug("opening " + Android.apps.dbnav.getPackageID() + " on " + Android.adb.getDeviceID());

			} catch (Exception e) {
				filename = Android.adb.setScreenShotFilename("starting BYOD Application");
				test.log(Status.FAIL, e);
				MyLogger.logger.error("app: " + Android.apps.dbnav.getPackageID() + " could not be started");
				Android.adb.saveScreeenShootOnServer(deviceID, filename, SCREEN_SHOTS_FOLDER);
				e.printStackTrace();

			}
		}
		else {
			MyLogger.logger.error("no free device found");
		//	test.log(Status.FAIL, "No free Device Found");
		}
	}
	
	@And("^I enter From Location as \"([^\"]*)\" To Location as \"([^\"]*)\"$")
	public void choose_env_login(String from,String to) throws Throwable {
	
		//select_env(username,password); 
		//dbctivity.click_cst_env();
		dbctivity.typeFromLocation(from);
	    dbctivity.typeToLocation(to);
	   // dbctivity.click_dev_env();
	   // dbctivity.click_cst_env();
		 /*if	(!cstenv.isChecked()) {
			  dbctivity.click_cst_env();
					
		 }else if(!devenv.isChecked()) {
			 dbctivity.click_dev_env();
			
		 }else if(!qatenv.isChecked()) {
			 dbctivity.click_qat_env();
			
		 }else if(!qatenv.isChecked()) {
			 dbctivity.click_pro_env();
			
		 }else {
			 MyLogger.logger.error("Unable to Check Environments");
		 }*/
		
	 
	}
	
	@Then("^I Clcik on Train Search$")
	public void clk_search() throws Throwable{
		dbctivity.clickSearchButton();
	}
	
	
	
}
