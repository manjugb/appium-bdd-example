package db.nav.api.apps.db;



import org.openqa.selenium.NoSuchElementException;

import db.nav.core.MyLogger;



public class DB {

	 

	
	private final DBUIObjects dbUIObjects;
	// private final String device_ID;

	public DB(String device_ID) {
		// this.device_ID = device_ID;
		dbUIObjects = new DBUIObjects(device_ID);
	}

	public void clickUIObject(String uiobjectIdentificator) {
		try {
			dbUIObjects.getUIObjecByText(uiobjectIdentificator).waitElementToappaer(3);
			dbUIObjects.getUIObjecByText(uiobjectIdentificator).tap();
			MyLogger.logger.info("clicked " + uiobjectIdentificator);
		} catch (Exception e) {
			MyLogger.logger.error(e.getMessage() + e.getStackTrace().toString());
			e.printStackTrace();
		}
	}

	public void scrollToElement(String uiobjectIdentificator) {
		try {
			if (!dbUIObjects.getUIObjecByText(uiobjectIdentificator).isElementEmpty()) {
				MyLogger.logger.info("Object was already visible no need to scroll");
			} else {
				MyLogger.logger.info("scrolling to  " + uiobjectIdentificator);
				dbUIObjects.getUIObjecByText(uiobjectIdentificator).scrollToElement();
				dbUIObjects.getUIObjecByText(uiobjectIdentificator).waitElementToappaer(15);
				MyLogger.logger.info("scrolled to  " + uiobjectIdentificator);
			}
		} catch (NoSuchElementException e) {
			dbUIObjects.getUIObjecByText(uiobjectIdentificator).scrollToElement();
			dbUIObjects.getUIObjecByText(uiobjectIdentificator).waitElementToappaer(3);

			MyLogger.logger.error(e.getMessage() + e.getStackTrace().toString());
			e.printStackTrace();
		}

	}

	public void clickUIObjectFoundByContentDesc(String uiobjectIdentificator) {
		try {
			dbUIObjects.getUIObjecByContentDesc(uiobjectIdentificator).waitElementToappaer(1);
			dbUIObjects.getUIObjecByContentDesc(uiobjectIdentificator).tap();
			MyLogger.logger.info("clicked " + uiobjectIdentificator);
		} catch (Exception e) {
			MyLogger.logger.error(e.getMessage() + e.getStackTrace().toString());
			e.printStackTrace();
		}

	}
	
	
	
	
	/*public void enter_UIObject(String userName) {
		try {
			dbUIObjects.getUIObjecByText(USER_NAME).waitElementToappaer(3);
			dbUIObjects.getUIObjecByText(USER_NAME).typeText(userName);
			MyLogger.logger.info("clicked " + USER_NAME);
		} catch (Exception e) {
			MyLogger.logger.error(e.getMessage() + e.getStackTrace().toString());
			e.printStackTrace();
		}
	}*/

	
	
	
}
