package db.nav.api.apps.db;

import java.util.Properties;

import org.openqa.selenium.By;

import db.nav.core.ADB;
import db.nav.core.MyLogger;
import db.nav.core.PropertiesFilesLoader;
import freemarker.template.utility.NullArgumentException;

public class DBUIelementsIdentificators {

	private static String propertiy_filename;
	private static Properties properties;
	private static String propertiy_filenames;
	private static Properties properties1;

	
    public static String CLICK_FROM="click_from";
    public static String CLICK_TO="click_to";
    public static String CLICK_SEARCH="click_search";
    

	public DBUIelementsIdentificators() {
		super();
	}

	public DBUIelementsIdentificators(String device_ID) {
		ADB adb = new ADB();
		String device_lang = adb.getDeviceLang(device_ID);
		propertiy_filename = getPropertiFileName(device_lang);
		properties = PropertiesFilesLoader.getInstance(propertiy_filename).loadPropertiesFile();
		MyLogger.logger.info(properties);
		
		propertiy_filenames = getPropertiFileNames(device_lang);
		properties1 = PropertiesFilesLoader.getInstance(propertiy_filenames).loadPropertiesFile();
		MyLogger.logger.info(properties1);
	}
   
	public String getElementIdentificatorByText(String key) {
		try {
			return "//android.widget.EditText[@text, '" + properties.getProperty(key) + "')]";
		} catch (NullPointerException e) {
			throw new NullArgumentException("properties variable in VerifoneUIelementsIdentificators is null");
		}
	}
	
	public String getElementIdentificatorByTextView(String key) {
		//android.widget.TextView
		
		try {
			return "//android.widget.TextView[@text, '" + properties.getProperty(key) + "')]";
		} catch (NullPointerException e) {
			throw new NullArgumentException("properties variable in VerifoneUIelementsIdentificators is null");
		}
	}
	
	//public String enter
	//android.widget.EditText[@text='User Name']
	public String getElementIdentificatorByContenDesc(String key) {
		try {
			return "//android.widget.ImageButton[@content-desc, '" + properties.getProperty(key) + "')]";
		} catch (NullPointerException e) {
			throw new NullArgumentException("properties variable in VerifoneUIelementsIdentificators is null");
		}
	}

	public String getElementIdentificatorByXpath(String key) {
		try {
											
			return properties.getProperty(key);
		} catch (NullPointerException e) {
			throw new NullArgumentException("properties variable in VerifoneUIelementsIdentificators is null");
		}
	}
	
	public String getElementIdentificatorClassName(String key) {
		try {
											
			return properties.getProperty(key);
		} catch (NullPointerException e) {
			throw new NullArgumentException("properties variable in VerifoneUIelementsIdentificators is null");
		}
	}
	
	
	
	
	public String getPropertiFileName(String device_lang) {
		if (device_lang.contains("en")) {
			MyLogger.logger.info("Reading Properties....!");
			return "en-US_verifone_locator.properties";
		}else {
			MyLogger.logger.info("Error Reading Properties");
			return null;
		}
	}
	
public String  getPropertiFileNames(String env) {
	switch(env)
	{
	   
	case "cst" :
		  MyLogger.logger.info("Reading CST Env Properties....!");
	      return "cst.propeties";
	      
	case "dev" :
		MyLogger.logger.info("Reading DEV Env Properties....!");
		   return "ev.propeties";
		   
	case "qat" :
		   MyLogger.logger.info("Reading QAT Env Properties....!");
		   return "qat.propeties";
		   
	case "pro" :
		   MyLogger.logger.info("Reading CST Env Properties....!");
		   return "qat.propeties";
	default:
		MyLogger.logger.info("Reading CST Env Properties....!");
        env = "Invalid Enviroment";
        return env;
    }
	//System.out.println(env);
}
		

	
	 
/*	public By getLocator(String ElementName) throws Exception {
		// Read value using the logical name as Key
		String locator = properties.getProperty(ElementName);
		// Split the value which contains locator type and locator value
		String locatorType = locator.split(":")[0];
		String locatorValue = locator.split(":")[1];
		// Return a instance of By class based on type of locator
		if (locatorType.toLowerCase().equals("id"))
			return By.id(locatorValue);
		else if (locatorType.toLowerCase().equals("name"))
			return By.name(locatorValue);
		else if ((locatorType.toLowerCase().equals("classname"))
				|| (locatorType.toLowerCase().equals("class")))
			return By.className(locatorValue);
		else if ((locatorType.toLowerCase().equals("tagname"))
				|| (locatorType.toLowerCase().equals("tag")))
			return By.className(locatorValue);
		else if ((locatorType.toLowerCase().equals("linktext"))
				|| (locatorType.toLowerCase().equals("link")))
			return By.linkText(locatorValue);
		else if (locatorType.toLowerCase().equals("partiallinktext"))
			return By.partialLinkText(locatorValue);
		else if ((locatorType.toLowerCase().equals("cssselector"))
				|| (locatorType.toLowerCase().equals("css")))
			return By.cssSelector(locatorValue);
		else if (locatorType.toLowerCase().equals("xpath"))
			return By.xpath(locatorValue);
		else
			throw new Exception("Locator type '" + locatorType
					+ "' not defined!!");
	}
*/


	
	

}
