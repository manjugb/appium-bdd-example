package db.nav.api.apps.db;
import org.apache.log4j.Level;

import com.aventstack.extentreports.ExtentTest;

import db.nav.api.apps.interfaces.Activity;
import db.nav.core.MyLogger;
import db.nav.core.UIObjectWrapper;

public class DBActivity implements Activity {

	private String deviceID = "";
	DBUIObjects dbUIObjects;
	private UIObjectWrapper uiobjec;
	private String filename;
	static ExtentTest test;

	public DBActivity(String deviceID) {
		this.deviceID = deviceID;
		dbUIObjects = new DBUIObjects(this.deviceID);
		MyLogger.logger.setLevel(Level.DEBUG);

	}

	public DBActivity() {
		MyLogger.logger.setLevel(Level.DEBUG);
	}

	@Override
	public void waitToLoad() {
		// TODO Auto-generated method stub
		waitForActivitUIElementstoAppaer();
	}

	private void waitForActivitUIElementstoAppaer() {
		dbUIObjects.getInputTxtFromLocation().waitElementToappaer(3);
		dbUIObjects.getInputTxtToLocation().waitElementToappaer(3);
		// .transactions_done_complete(5);
		// dbUIObjects.getClickDebitCredit().waitElementToappaer(3);

	}

	public void typeFromLocation(String username) throws Throwable {
		// Thread.sleep(500);

		dbUIObjects.getInputTxtFromLocation().typeText(username);
		MyLogger.logger.info("Entered the username");

	}

	public void typeToLocation(String password) throws Throwable {
		// Thread.sleep(500);
		dbUIObjects.getInputTxtToLocation().typeText(password);
	}

	public void clickSearchButton() throws Throwable {
		Thread.sleep(500);

		dbUIObjects.clickOnTrainSeach().tap();}

	

	// verify remember me
	/*public void verify_rememberMe_text(String remMeText) throws Throwable {

		String actremMeText = dbUIObjects.verifyRememberText().getText();
		MyLogger.logger.info("Remember Message:" + actremMeText);
		if (actremMeText.equalsIgnoreCase(remMeText)) {
			assertEquals(actremMeText, remMeText);
		} else {
			assertNotEquals(remMeText, actremMeText);
		}
	}

	public void verify_env_text(String remMeText) throws Throwable {

		String actremMeText = dbUIObjects.verifyEnvironmentText().getText();
		MyLogger.logger.info("Remember Message:" + actremMeText);
		if (actremMeText.equalsIgnoreCase(remMeText)) {
			assertEquals(actremMeText, remMeText);
		} else {
			assertNotEquals(remMeText, actremMeText);
		}
	}

	public void clickEnvSwipe() {
		dbUIObjects.clickEnvironment().tap();
	}

	public void click_alert_ok() throws Throwable {
		dbUIObjects.click_ok().tap();
	}

	public void clickRememberMe() throws Throwable {

		dbUIObjects.getClickRememberMe().tap();
	}

	public void clickMenu() throws Throwable {
		Thread.sleep(500);
		dbUIObjects.getClickMainMenu().tap();
	}

	public void clkdebitcredit() throws Throwable {
		// Thread.sleep(500);
		dbUIObjects.getClickDebitCredit().tap();
	}

	public void enter_MID(String mid) throws Throwable {
		dbUIObjects.getInputMID().typeText(mid);
	}

	public void enter_TID(String tid) throws Throwable {
		dbUIObjects.getInputTID().typeText(tid);
	}

	public void click_save_merchant_details() throws Throwable {
		dbUIObjects.getClkSaveMerchantPopup().tap();
	}

	public void click_on_settongs() throws Throwable {
		dbUIObjects.getClickOnSettings().tap();
	}

	public void click_on_connect() throws Throwable {
		dbUIObjects.getClickOnScanAndConnect().isFocused();
		dbUIObjects.getClickOnScanAndConnect().tap();
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clk_blue_device() throws InterruptedException {
		// dbUIObjects.getclkWpsDevice().isFocused();
		dbUIObjects.getclkWpsDevice().tap();
		// uiobjec.alertSwitch();

	}

	public void clk_key_0() throws InterruptedException {

		dbUIObjects.getclk_Key_0().tap();
	}

	public void clk_key_1() throws InterruptedException {

		dbUIObjects.getclk_Key_1().tap();
	}

	public void clk_key_2() throws InterruptedException {

		dbUIObjects.getclk_Key_2().tap();
	}

	public void clk_key_3() throws InterruptedException {

		dbUIObjects.getclk_Key_3().tap();
	}

	public void clk_key_4() throws InterruptedException {

		dbUIObjects.getclk_Key_4().tap();
	}

	public void clk_key_5() throws InterruptedException {

		dbUIObjects.getclk_Key_5().tap();
	}

	public void clk_key_6() throws InterruptedException {

		dbUIObjects.getclk_Key_6().tap();
	}

	public void clk_key_7() throws InterruptedException {

		dbUIObjects.getclk_Key_7().tap();
	}

	public void clk_key_8() throws InterruptedException {

		dbUIObjects.getclk_Key_8().tap();
	}

	public void clk_key_9() throws InterruptedException {

		dbUIObjects.getclk_Key_9().tap();
	}

	public void clk_key_c() throws InterruptedException {

		dbUIObjects.getclk_Key_c().tap();
	}

	public void clk_key_x() throws InterruptedException {

		dbUIObjects.getclk_Key_x().tap();
	}

	public void click_amout_charge() throws Throwable {
		dbUIObjects.getclkonChargeAmountLinkTab().tap();
	}

	public void click_change_currency() throws Throwable {
		dbUIObjects.getchangeCurrency().tap();
	}

	public void select_currency() throws Throwable {
		dbUIObjects.selectCurrency().tap();
	}

	public void close_currency_ppup() throws Throwable {
		dbUIObjects.closeCurrency_popup().tap();
	}

	public void verify_transaciton_message(String transactMessage) throws InterruptedException {
		Thread.sleep(500);
		String acttransactMessage = dbUIObjects.transactions_complete_msg().getText();
		if (acttransactMessage.equalsIgnoreCase(transactMessage)) {
			assertEquals(acttransactMessage, transactMessage);
			MyLogger.logger.info("Transacation Done" + acttransactMessage);
		} else {
			assertNotEquals(transactMessage, acttransactMessage);
			MyLogger.logger.info("Error in Transacation Done" + acttransactMessage);

		}
	}

	public void click_transaction_done() throws InterruptedException {
		Thread.sleep(500);
		dbUIObjects.transactions_done_complete().tap();
	}
	
	public void click_transaction_done_purchase() throws InterruptedException {
		Thread.sleep(500);
		dbUIObjects.transactions_done_complete_purchase_tip().tap();
	}
	
	
	
	public void verify_transaction_complete_msg(String doneMessage) throws Throwable {

		String actdoneMessage = dbUIObjects.transactions_complete_msg().getText();
		MyLogger.logger.info("Confirm Message:" + actdoneMessage);
		assertEquals(actdoneMessage, doneMessage);
	}

	public void click_email() throws Throwable {
		dbUIObjects.click_email().tap();
	}

	public void email_confirmation_screen() throws Throwable {
		dbUIObjects.continue_confirmation_screen().tap();
	}

	public void enter_mail_screen(String email) throws Throwable {
		dbUIObjects.enter_mail_screen().typeText(email);
	}

	public void email_continue_send() throws Throwable {
		dbUIObjects.enter_email_screen_continue().tap();
	}

	public void verify_email(String emalid) throws Throwable {
		String actemail = dbUIObjects.verify_email().getText();
		if (actemail.equalsIgnoreCase(emalid)) {
			assertEquals(emalid, actemail);
		} else {
			assertNotEquals(emalid, actemail);
		}
	}

	public void email_confirm_but() throws Throwable {
		dbUIObjects.email_confim_button().tap();
	}

	public void click_amount_number(int key) throws Throwable {
		dbUIObjects.key_enter().tapListElement(key);
	}

	public void verify_email_confirm(String plsConfirmmsg) throws Throwable {
		plsConfirmmsg = " " + plsConfirmmsg;
		String actplsconfirm = dbUIObjects.verify_please_confirm_message().getText();
		MyLogger.logger.info("Confirm Message:" + actplsconfirm);
		if (actplsconfirm.equalsIgnoreCase(actplsconfirm)) {
			assertEquals(actplsconfirm, plsConfirmmsg);
		} else {
			assertNotEquals(plsConfirmmsg, actplsconfirm);
			;
		}

	}

	public void verify_lable_your_email(String yourEmail) throws Throwable {
		String actYourEmail = dbUIObjects.verify_your_email_address().getText();
		yourEmail = " " + yourEmail + " ";
		if (actYourEmail.equalsIgnoreCase(yourEmail)) {
			assertEquals(actYourEmail, yourEmail);
			MyLogger.logger.info("Your Email found:" + actYourEmail);
		} else {
			assertNotEquals(yourEmail, actYourEmail);
		}

	}

	public void verify_email_sent_message(String emailSentMessage) throws Throwable {
		String actYourEmailSentMessage = dbUIObjects.verify_email_sent_message().getText();
		if (actYourEmailSentMessage.equalsIgnoreCase(emailSentMessage))
			assertEquals(actYourEmailSentMessage, emailSentMessage);
	}

	public void click_on_don_email_sent_screen() throws Throwable {
		dbUIObjects.click_on_don_email_sent_screen().tap();
	}

	// Purchage Tip

	public void click_on_purchage_tip() throws Throwable {
		dbUIObjects.getclk_purchase_tip().tap();
	}

	public void verify_5_percent_amount(String fiveAmount) {
		String actfiveAmount = dbUIObjects.verify_5_percent_amount().getText();
		if (actfiveAmount.equalsIgnoreCase(fiveAmount)) {
			assertEquals(actfiveAmount, fiveAmount);
		} else {
			assertNotEquals(fiveAmount, actfiveAmount);
			;
			filename = Android.adb.setScreenShotFilename("fivePercentAmount");
			// MyLogger.logger.error("app: " + Android.apps.vfl.getPackageID() + " could not
			// be started");
			Android.adb.saveScreeenShootOnServer(deviceID, filename, SCREEN_SHOTS_FOLDER);

		}

	}

	public void click_on_5_tip() throws Throwable {
		dbUIObjects.click_on_5_percen().tap();
	}

	public void verify_10_percent_amount(String tenAmount) throws Throwable {
		String acttenAmount = dbUIObjects.verify_10_percent_amount().getText();
		if (acttenAmount.equalsIgnoreCase(tenAmount)) {
			assertEquals(acttenAmount, tenAmount);
		} else {
			assertNotEquals(tenAmount, acttenAmount);
			;
			filename = Android.adb.setScreenShotFilename("tenPercentAmount");
			// MyLogger.logger.error("app: " + Android.apps.vfl.getPackageID() + " could not
			// be started");
			Android.adb.saveScreeenShootOnServer(deviceID, filename, SCREEN_SHOTS_FOLDER);

		}

	}

	public void click_on_10_tip() throws Throwable {
		dbUIObjects.click_on_10_percen().tap();
	}

	public void verify_20_percent_amount(String twenAmount) {
		String acttwenAmount = dbUIObjects.verify_20_percent_amount().getText();
		if (acttwenAmount.equalsIgnoreCase(twenAmount)) {
			assertEquals(twenAmount, acttwenAmount);
			;
		} else {
			assertNotEquals(twenAmount, acttwenAmount);
			;
			filename = Android.adb.setScreenShotFilename("TwentyPercentAmount");
			// MyLogger.logger.error("app: " + Android.apps.vfl.getPackageID() + " could not
			// be started");
			Android.adb.saveScreeenShootOnServer(deviceID, filename, SCREEN_SHOTS_FOLDER);

		}

	}

	public void click_on_20_tip() throws Throwable {
		dbUIObjects.click_on_20_percen().tap();
	}

	public void click_others_tip() throws Throwable {
		dbUIObjects.click_on_others().tap();
	}

	public void click_enter_button() throws Throwable {
		dbUIObjects.click_on_others_enter_button().tap();
	}

	public void click_enter_cancel_button() throws Throwable {
		dbUIObjects.click_on_others_enter_cancel_button().tap();
	}

	public void click_no_tip_buton() throws Throwable {
		dbUIObjects.click_on_no_tip_button().tap();
	}

	public void click_cancel_no_tip() {
		dbUIObjects.click_cancle_tip_button().tap();
	}

	public void click_none_but() throws Throwable {
		dbUIObjects.click_none_send().tap();
	}

	// Refund
	public void clkTransactionList() throws Throwable {
		dbUIObjects.click_on_transaction_list().tap();
	}

	public void clkSale() throws Throwable {
		dbUIObjects.click_sale().tap();
	}

	public void clkOnRefund() throws Throwable {
		dbUIObjects.click_on_refund().tap();
	}

	public void chooseReason(int itemIndex) throws Throwable {
		dbUIObjects.select_item_index().getElementsList().get(itemIndex);
	}

	public void processRefund() throws Throwable {
		dbUIObjects.process_refund().tap();
	}

	public void partialRefund() throws Throwable {
		dbUIObjects.partial_refund().tap();
	}

	public void click_partial_key1() throws Throwable {
		dbUIObjects.partial_refund_key_1().tap();
	}

	public void click_partial_key2() throws Throwable {
		dbUIObjects.partial_refund_key_2().tap();
	}

	public void click_partial_key3() throws Throwable {
		dbUIObjects.partial_refund_key_3().tap();
	}

	public void click_partial_key4() throws Throwable {
		dbUIObjects.partial_refund_key_4().tap();
	}

	public void click_partial_key5() throws Throwable {
		dbUIObjects.partial_refund_key_5().tap();
	}

	public void click_partial_key6() throws Throwable {
		dbUIObjects.partial_refund_key_6().tap();
	}

	public void click_partial_key7() throws Throwable {
		dbUIObjects.partial_refund_key_7().tap();
	}

	public void click_partial_key8() {
		dbUIObjects.partial_refund_key_8().tap();
	}

	public void click_partial_key9() throws Throwable {
		dbUIObjects.partial_refund_key_9().tap();
	}

	public void click_partial_keyc() throws Throwable {
		dbUIObjects.partial_refund_key_c().tap();
	}

	public void click_partial_key0() throws Throwable {
		dbUIObjects.partial_refund_key_0().tap();
	}

	// click on partial accept
	public void click_partial_accept() throws Throwable {
		dbUIObjects.partial_refund_accept().tap();
	}

	// click on Activity
	public void clickActivity() throws Throwable {
		dbUIObjects.click_on_activity().tap();
	}

	// verify amount in card entry screen
	public void verif_amount_pay(String amountPay) throws Throwable {
		String actAmountPay = dbUIObjects.verify_amount_pay().getText().toString();
		MyLogger.logger.info("Amount Matched:" + actAmountPay);
		//assertTrue(actAmountPay.contains(amountPay));
		if (actAmountPay.contains(amountPay)) {
			assertTrue(actAmountPay.contains(amountPay));
			MyLogger.logger.info("Amount Matched:" + actAmountPay);
		} else {
			assertNotEquals(amountPay, actAmountPay);
			MyLogger.logger.info("Amount Not Matched:" + actAmountPay);
		}
	}
	// card cancel Pay
	public void cardCancelPay() throws Throwable {
		dbUIObjects.card_cancel_pay().tap();
	}

	// card cancel ok in card entry screen
	public void card_cancel_ok_but() throws Throwable {
		dbUIObjects.clk_transaction_cancel_ok_but().tap();
	}

	public void verify_trans_cancle_msg(String transcanmsg) throws Throwable {
		
		String acttranscanmsg = dbUIObjects.ver_transaction_cancel_msg().getText();
		//MyLogger.logger.info("Actual:"+acttranscanmsg);
		//assertEquals(transcanmsg, acttranscanmsg);
		if (transcanmsg.equals(acttranscanmsg)) {
			assertEquals(transcanmsg, acttranscanmsg);
			MyLogger.logger.info("Transcation Canclel Message Matched:" + acttranscanmsg);
		} else {
			assertNotEquals(transcanmsg, acttranscanmsg);
			MyLogger.logger.info("Transcation Canclel Message:" + acttranscanmsg);
		}

	}

	// configuration firmware message
	public void verify_config_firmware_msg(String firmwaremsg) throws Throwable {
		String actfirmwaremsg = dbUIObjects.check_config_firmware_msg().getText();
		firmwaremsg = firmwaremsg + " ";
		//assertEquals(actfirmwaremsg, firmwaremsg);
		if (actfirmwaremsg.equalsIgnoreCase(firmwaremsg)) {
			assertEquals(actfirmwaremsg, firmwaremsg);
			MyLogger.logger.info("Matched Config&Firmware Message:" + actfirmwaremsg);
		} else {
			assertNotEquals(firmwaremsg, actfirmwaremsg);
			 MyLogger.logger.info("Not Matched Config&Firmware Message:" +actfirmwaremsg+":" +firmwaremsg+"");
		}

	}

	// click refund reason list
	public void clk_refund_reason_list() throws Throwable {
		dbUIObjects.click_on_list().tap();
	}

	// click on refund goods
	public void clk_refund_reason_goods() throws Throwable {
		dbUIObjects.returned_goods().tap();
	}

	public void clk_refund_reason_accedental() throws Throwable {
		dbUIObjects.accedental_charge().tap();
	}

	public void clk_refund_reason_others() throws Throwable {
		dbUIObjects.select_others().tap();
	}

	public void clk_cancel_order() throws Throwable {
		dbUIObjects.cancel_order().tap();
	}

//Change other currency
	public void clk_change_currency_usd() throws Throwable {
		dbUIObjects.change_currency_usd().tap();
	}

	public void clk_change_currency_pound() throws Throwable {
		dbUIObjects.change_currency_pound().tap();
	}

	// change currency error on card entry screen
	public void er_change_currency_err(String errmsg) throws Throwable {
		String acterrmsg = dbUIObjects.change_currency_err().getText();
		if (acterrmsg.equalsIgnoreCase(errmsg)) {
			assertEquals(acterrmsg, errmsg);
			MyLogger.logger.info("Transaction Message Matched:" + errmsg);
		} else {
			assertNotEquals(errmsg, acterrmsg);
			MyLogger.logger.info("Transaction Message not Matched:" + acterrmsg + ":" + errmsg + "");
		}

	}

	// Disconnect WPS

	public void click_wps_disconnect() throws Throwable {
		dbUIObjects.disconnect_wps().tap();
	}

	public void alert_msg(String disconnectmsg) {
		String actdisconnectmsg = dbUIObjects.alert_msg().getText();
		if (actdisconnectmsg.equalsIgnoreCase(disconnectmsg)) {
			assertEquals(actdisconnectmsg, disconnectmsg);
			MyLogger.logger.info("Transaction Message Matched:" + disconnectmsg);
		} else {
			assertNotEquals(disconnectmsg, actdisconnectmsg);
			MyLogger.logger.info("Transaction Message not Matched:" + actdisconnectmsg + ":" + disconnectmsg + "");
		}
	}

	public void alert_msg_ok_but() throws Throwable {
		dbUIObjects.alert_msg_ok_but().tap();
	}

	// original amount
	public void ver_original_amount_refund_screen(String origAmount) throws Throwable {
		String actOriginalamt = dbUIObjects.original_amount_refund_screen().getText();
		if (actOriginalamt.equalsIgnoreCase(origAmount)) {
			assertEquals(actOriginalamt, origAmount);
			MyLogger.logger.info("Original Amount  Matched:" + origAmount);
		} else {
			assertNotEquals(origAmount, actOriginalamt);
			MyLogger.logger.info("Original Amount not Matched:" + actOriginalamt + ":" + origAmount + "");
		}

	}

	// refund amount
	public void ver_refund_amount_refund_screen(String refundAmount) throws Throwable {
		String actrefundAmount = dbUIObjects.refund_amount().getText();
		if (actrefundAmount.equalsIgnoreCase(refundAmount)) {
			assertEquals(actrefundAmount, refundAmount);
			MyLogger.logger.info("Refound Amount  Matched:" + refundAmount);
		} else {
			assertNotEquals(refundAmount, actrefundAmount);
			MyLogger.logger.info("Refound Amount not Matched:" + actrefundAmount + ":" + refundAmount + "");
		}

	}

	// refund total
	public void ver_refund_total(String refundTotAmount) throws Throwable {
		String actrefundTotAmount = dbUIObjects.refund_total().getText();
		if (actrefundTotAmount.equalsIgnoreCase(refundTotAmount)) {
			assertEquals(actrefundTotAmount, refundTotAmount);
			MyLogger.logger.info("Refound Total Amount  Matched:" + refundTotAmount);
		} else {
			assertNotEquals(refundTotAmount, actrefundTotAmount);
			MyLogger.logger.info("Refound Total Amount not Matched:" + actrefundTotAmount + ":" + refundTotAmount + "");
		}

	}

	// approved message
	public void ver_aproved_msg(String aprovedMsg) throws Throwable {
		// String acttranscanmsg =
		// dbUIObjects.ver_transaction_cancel_msg().getText();
		String actraprovedMsg = dbUIObjects.refund_approved_msg().getText();
		if (actraprovedMsg.equalsIgnoreCase(aprovedMsg)) {
			assertEquals(actraprovedMsg, aprovedMsg);
			MyLogger.logger.info("Aproved  Matched:" + actraprovedMsg);
		} else {
			assertEquals(actraprovedMsg, aprovedMsg);
		}
		MyLogger.logger.info("Aproved Not Matched:" + actraprovedMsg);

	}

	// approved ok button
	public void refund_approved_msg_ok_but() throws Throwable {
		dbUIObjects.refund_approved_msg_ok_but().tap();
	}

	public void ver_please_wait_transaction(String plswaittrans) throws Throwable {
		String actplswaittrans = dbUIObjects.please_wait_Tranasaction_process_msg().getText();
		if (actplswaittrans.equalsIgnoreCase(plswaittrans)) {
			assertEquals(actplswaittrans, plswaittrans);
			MyLogger.logger.info("Refound Total Amount  Matched:" + plswaittrans);
		} else {
			assertNotEquals(plswaittrans, actplswaittrans);
			MyLogger.logger.info("Refound Total Amount not Matched:" + actplswaittrans + ":" + plswaittrans + "");
		}

	}

	public void ver_please_wait_screen_scan_connect(String plsscanconnect) throws Throwable {
		String actplsscanconnect = dbUIObjects.please_wait_screen_scan_connect().getText();
		if (actplsscanconnect.equalsIgnoreCase(plsscanconnect)) {
			assertEquals(actplsscanconnect, plsscanconnect);
			MyLogger.logger.info("Please Wait  Matched:" + plsscanconnect);
		} else {
			assertNotEquals(plsscanconnect, actplsscanconnect);
			MyLogger.logger.info("Please Wait not Matched:" + actplsscanconnect + ":" + plsscanconnect + "");
		}

	}

	public void click_on_about_but() throws Throwable {
		dbUIObjects.click_on_about_but().tap();
	}

	public void ver_version(String ver) throws Throwable {
		String actver = dbUIObjects.version().getText();
		if (actver.equalsIgnoreCase(ver)) {
			assertEquals(actver, ver);
			MyLogger.logger.info("Version  Matched:" + ver);
		} else {
			assertNotEquals(ver, actver);
			MyLogger.logger.info("Version not Matched:" + actver + ":" + ver + "");
		}
	}

	public void ver_logical_device_id(String logiclId) throws Throwable {
		String actlogiclId = dbUIObjects.logical_device_id().getText();
		if (actlogiclId.equalsIgnoreCase(logiclId)) {
			assertEquals(actlogiclId, logiclId);
			MyLogger.logger.info("Logical Device  Matched:" + logiclId);
		} else {
			assertNotEquals(logiclId, actlogiclId);
			MyLogger.logger.info("Logial Device not Matched:" + actlogiclId + ":" + logiclId + "");
		}

	}

	public void ver_hardware_device_id(String hardwaredeviceId) throws Throwable {
		String acthardwaredeviceId = dbUIObjects.hardware_device_id().getText();
		if (acthardwaredeviceId.equalsIgnoreCase(hardwaredeviceId)) {
			assertEquals(acthardwaredeviceId, hardwaredeviceId);
			MyLogger.logger.info("Hardware Device Id  Matched:" + hardwaredeviceId);
		} else {
			assertNotEquals(hardwaredeviceId, acthardwaredeviceId);
			MyLogger.logger.info("Hardware DeciceId not Matched:" + acthardwaredeviceId + ":" + hardwaredeviceId + "");
		}

	}
	
	public void ver_firmware_ver(String firmawareVer) throws Throwable {
		String actfirmawareVer = dbUIObjects.firmware_ver().getText();
		if (actfirmawareVer.equalsIgnoreCase(firmawareVer)) {
			assertEquals(actfirmawareVer, firmawareVer);
			MyLogger.logger.info("Firmware Version  Matched:" + firmawareVer);
		} else {
			assertNotEquals(firmawareVer, actfirmawareVer);
			MyLogger.logger.info("Firmware Version not Matched:" + actfirmawareVer + ":" + firmawareVer + "");
		}

	}
	
	
	public void ver_merchant_tid(String tid) throws Throwable {
		String acttid = dbUIObjects.merchant_tid().getText();
		if (acttid.equalsIgnoreCase(tid)) {
			assertEquals(acttid, tid);
			MyLogger.logger.info("TID  Matched:" + tid);
		} else {
			assertNotEquals(tid, acttid);
			MyLogger.logger.info("TID not Matched:" + acttid + ":" + tid + "");
		}

	}
	
	
	public void ver_app_name(String appname) throws Throwable {
		String actappname = dbUIObjects.app_name().getText();
		if (actappname.equalsIgnoreCase(appname)) {
			assertEquals(actappname, appname);
			MyLogger.logger.info("AppName  Matched:" + appname);
		} else {
			assertNotEquals(appname, actappname);
			MyLogger.logger.info("AppName not Matched:" + actappname + ":" + appname + "");
		}

	}
	
	public void ver_app_version(String appver) throws Throwable {
		String actappver = dbUIObjects.app_version().getText();
		if (actappver.equalsIgnoreCase(appver)) {
			assertEquals(actappver, appver);
			MyLogger.logger.info("App Version  Matched:" + appver);
		} else {
			assertNotEquals(appver, actappver);
			MyLogger.logger.info("App Version not Matched:" + actappver + ":" + appver + "");
		}

	}
	
	public void ver_merchat_mid(String mid) throws Throwable {
		String actmid = dbUIObjects.merchat_mid().getText();
		if (actmid.equalsIgnoreCase(mid)) {
			assertEquals(actmid, mid);
			MyLogger.logger.info("TID  Matched:" + mid);
		} else {
			assertNotEquals(mid, actmid);
			MyLogger.logger.info("TID not Matched:" + actmid + ":" + mid + "");
		}

	}
	
	public void ver_configuration(String configver) throws Throwable {
		String actconfigver = dbUIObjects.configuration().getText();
		if (actconfigver.equalsIgnoreCase(configver)) {
			assertEquals(actconfigver, configver);
			MyLogger.logger.info("Configuration Version  Matched:" + configver);
		} else {
			assertNotEquals(configver, actconfigver);
			MyLogger.logger.info("Configuration Version not Matched:" + actconfigver + ":" + configver + "");
		}

	}
	
	
	public void other_amount_key_1() throws Throwable {
		dbUIObjects.other_amount_key_1().tap();
	}

	public void other_amount_key_2() throws Throwable {
		dbUIObjects.other_amount_key_2().tap();
	}

	public void other_amount_key_3() throws Throwable {
		dbUIObjects.other_amount_key_3().tap();
	}

	public void other_amount_key_4() throws Throwable {
		dbUIObjects.other_amount_key_4().tap();
	}

	public void other_amount_key_5() throws Throwable {
		dbUIObjects.other_amount_key_5().tap();
	}

	public void other_amount_key_6() throws Throwable {
		dbUIObjects.other_amount_key_6().tap();
	}

	public void other_amount_key_7() throws Throwable {
		dbUIObjects.other_amount_key_7().tap();
	}

	public void other_amount_key_8() {
		dbUIObjects.other_amount_key_8().tap();
	}

	public void other_amount_key_9() throws Throwable {
		dbUIObjects.other_amount_key_9().tap();
	}

	public void other_amount_key_c() throws Throwable {
		dbUIObjects.other_amount_key_c().tap();
	}

	public void other_amount_key_0() throws Throwable {
		dbUIObjects.other_amount_key_0().tap();
	}
	
	public void other_amount_key_x() throws Throwable {
		dbUIObjects.other_amount_key_x().tap();
	}
	
	
	public void click_void_but() throws Throwable {
		dbUIObjects.click_void_but().tap();
	}

	public void click_eReceipt_but() throws Throwable {
		dbUIObjects.click_eReceipt_but().tap();
	}
	
	public void email_cancel_but() throws Throwable {
		dbUIObjects.email_cancel_but().tap();
	}
	
	
	
	public void click_cst_env() throws Throwable {
		if(!dbUIObjects.check_box_cst_env().isChecked()) {
		 dbUIObjects.check_box_cst_env().tap();
				
		}else {
			dbUIObjects.check_box_cst_env().tap();
			MyLogger.logger.error("Error click "+dbUIObjects.check_box_cst_env()+"");}
		}
	
	
	 
	
	public void click_dev_env() throws Throwable {
		if(!dbUIObjects.check_box_dev_env().isChecked()) {
		dbUIObjects.check_box_dev_env().tap();
	}else {
		MyLogger.logger.error("Error click "+dbUIObjects.check_box_cst_env()+"");}
	}

	public void click_qat_env() throws Throwable {
		dbUIObjects.check_box_qat_env().tap();
	}
	
	public void click_pro_env() throws Throwable {
		dbUIObjects.check_box_pro_env().tap();
	}
	
	
	public void e_receipt_enter_mail(String eRmail) throws Throwable {
		dbUIObjects.e_receipt_enter_mail().typeText(eRmail);
	}
	
	public void e_reciept_continue() throws Throwable {
		dbUIObjects.e_reciept_continue().tap();
	}
	

	public void e_receipt_cancle() throws Throwable {
		dbUIObjects.e_receipt_cancle().tap();
	}
	
//Email-Purchase 
	public void Enter_Mail_Screen_enter_mail_purchase(String email) throws Throwable {
		dbUIObjects.Enter_Mail_Screen_enter_mail_purchase().typeText(email);
	}
	
	public void continue_confimation_screen_purchase() throws Throwable {
		dbUIObjects.continue_confimation_screen_purchase().tap();
	}
	
	
	public void Enter_Email_Screen_continue_purchase() throws Throwable {
		dbUIObjects.Enter_Email_Screen_continue_purchase().tap();
	}
	
	public void verify_email_id_purchase(String emailId) throws Throwable {
		String actemailid = dbUIObjects.verify_email_id_purchase().getText();
		if (actemailid.equalsIgnoreCase(emailId)) {
			assertEquals(actemailid, emailId);
			MyLogger.logger.info("Configuration Version  Matched:" + emailId);
		} else {
			assertNotEquals(emailId, actemailid);
			MyLogger.logger.info("Configuration Version not Matched:" + actemailid + ":" + emailId + "");
		}

	}
	
	public void email_confirm_button_purchase() throws Throwable {
		dbUIObjects.email_confirm_button_purchase().tap();
	}
	

	public void transaction_completed_msg_purchase(String transmsg) throws Throwable {
		
		
		String actmsg = dbUIObjects.transaction_completed_msg_purchase().getText();
		if (actmsg.equalsIgnoreCase(transmsg)) {
			assertEquals(actmsg, transmsg);
			MyLogger.logger.info("Transaction  Matched:" + transmsg);
		} else {
			assertNotEquals(transmsg, actmsg);
			MyLogger.logger.info("Transaction not Matched:" + actmsg + ":" + transmsg + "");
		}
	}



	
	public void transcation_success_done_purchase() throws Throwable {
		dbUIObjects.transcation_success_done_purchase().tap();
	}
	
	
	//EMAIL PURCHASE VALIDATION
	
public void please_confirm_message_purchase(String transmsg) throws Throwable {
		
		
		String actmsg = dbUIObjects.please_confirm_message_purchase().getText();
		if (actmsg.equalsIgnoreCase(transmsg)) {
			assertEquals(actmsg, transmsg);
			MyLogger.logger.info("Transaction  Matched:" + transmsg);
		} else {
			assertNotEquals(transmsg, actmsg);
			MyLogger.logger.info("Transaction not Matched:" + actmsg + ":" + transmsg + "");
		}
	}

public void your_email_address_purchase(String transmsg) throws Throwable {
	
	
	String actmsg = dbUIObjects.your_email_address_purchase().getText();
	if (actmsg.equalsIgnoreCase(transmsg)) {
		assertEquals(actmsg, transmsg);
		MyLogger.logger.info("Transaction  Matched:" + transmsg);
	} else {
		assertNotEquals(transmsg, actmsg);
		MyLogger.logger.info("Transaction not Matched:" + actmsg + ":" + transmsg + "");
	}
}
public void email_sent_message_purchase(String transmsg) throws Throwable {
	
	
	String actmsg = dbUIObjects.email_sent_message_purchase().getText();
	if (actmsg.equalsIgnoreCase(transmsg)) {
		assertEquals(actmsg, transmsg);
		MyLogger.logger.info("Transaction  Matched:" + transmsg);
	} else {
		assertNotEquals(transmsg, actmsg);
		MyLogger.logger.info("Transaction not Matched:" + actmsg + ":" + transmsg + "");
	}
}*/
	
}


	
	

