package db.nav.api.apps.db;


import db.nav.core.UIObjectWrapper;
import db.nav.core.UISelectorWrapper;
import static db.nav.api.apps.db.DBUIelementsIdentificators.CLICK_FROM;
import static db.nav.api.apps.db.DBUIelementsIdentificators.CLICK_TO;
import static db.nav.api.apps.db.DBUIelementsIdentificators.CLICK_SEARCH;

public class DBUIObjects {

	private final String device_ID;
	private final DBUIelementsIdentificators dbUIelementsIdentificators;
	private String locator;

	//

	private static UIObjectWrapper uiobjec,input_from_loc,input_to_loc,clk_search;
	                               

	public DBUIObjects(String device_Id) {
		this.device_ID = device_Id;
		dbUIelementsIdentificators = new DBUIelementsIdentificators(device_Id);

	}
	
	public UIObjectWrapper getUIObjecByText(String identificator) {
		locator = dbUIelementsIdentificators.getElementIdentificatorByText(identificator);
		return new UISelectorWrapper(this.device_ID).xPath(locator).makeIUObject();
	}

	public UIObjectWrapper getUIObjecByContentDesc(String identificator) {
		locator = dbUIelementsIdentificators.getElementIdentificatorByContenDesc(identificator);
		return new UISelectorWrapper(this.device_ID).xPath(locator).makeIUObject();
	}

	public UIObjectWrapper getUIObjecByXpath(String identificator) {
		locator = dbUIelementsIdentificators.getElementIdentificatorByXpath(identificator);
		return new UISelectorWrapper(this.device_ID).xPath(locator).makeIUObject();
	}

	public UIObjectWrapper getInputTxtFromLocation() {
		locator = dbUIelementsIdentificators.getElementIdentificatorByText(CLICK_FROM);
		input_from_loc = new UISelectorWrapper(this.device_ID).text(locator).makeIUObject();
		return input_from_loc;
	}

	public UIObjectWrapper getInputTxtToLocation() {
		locator = dbUIelementsIdentificators.getElementIdentificatorByXpath(CLICK_TO);
		input_to_loc = new UISelectorWrapper(this.device_ID).text(locator).makeIUObject();
		return input_to_loc;
	}
	
	public UIObjectWrapper clickOnTrainSeach() {
		locator = dbUIelementsIdentificators.getElementIdentificatorByXpath(CLICK_SEARCH);
		clk_search = new UISelectorWrapper(this.device_ID).text(locator).makeIUObject();
		return clk_search;
	}
	
	
	


}
