package db.nav.api.apps.interfaces;
/**
 * 
 * @author Manjunath Golla Bala
 * {@summary} this interface defines launching device,forcestop,clearAppData,getApkPath
 *
 */

public interface Application {

	public Object open(String deviceID);
	//public Object install(String deviceID);

	public void forceStop(String deviceID);

	public String getPackageID();

	public String getLuncherActivityID();

	public void clearAppData(String deviceID);

	public String getApkPath();
	

	

}
