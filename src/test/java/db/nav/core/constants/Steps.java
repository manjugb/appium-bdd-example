package db.nav.core.constants;
/**
 * 
 * @author Manjunath
 * {@summary} this interface defines steps definition where used in Runner File
 *
 */

public interface Steps {
	
	String DB_STEPS_DEFINITIONS = "db.nav.api.apps.testSteps";

	
}
