package db.nav.core.constants;
/**
 * 
 * @author Manjunath Golla Bala
 * {@summary} this interface defines features path values and where used in Test runner file
 *
 */

public interface Features {
	
	//Sample Example
	//String VERIFONE_FEATURES = "src/test/resources/BYOD/ .feature";
    String DB_APP="src/test/resources/DB";
    
}
