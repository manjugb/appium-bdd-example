package db.nav.core.constants;

public interface Constants {
	public static final String PORTS[] = { "4723", "4726", "4729", "4732", "4735", "4738" };

	public static String URL_STRINGS[] = { "http://127.0.0.1:4723/wd/hub" };
	public static final String URL_SUFFIX = "/wd/hub";
	//public static final String HTML_RESULT_FOLDER = "/htmlResults";
	public static final String LOCAL_HOST = "http://127.0.0.1";
	// public static final String LOCAL_HOST = "http://10.0.2.2:8080";

	public static final String BBPOS_APK = "byod_app-debug.apk"; //apk file
		public static final String PATH = "D:\\apks"; //apk path
	public static final String PROPERTIES_FILE_FOLDER = "properties_files"; // src/main/resources/

	// public static final Map<String[], String[]> APP_APK_PATH = new
	// HashMap<String[], String[]>(){PATH,APP_NAME};

}
