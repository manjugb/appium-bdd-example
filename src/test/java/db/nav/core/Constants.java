package db.nav.core;
/**
 * 
 * @author Manjunath Golla Bala
 * @{summary} this interface defines global constants were used reports
 *
 */
// global static constant are defined in this interface i.e.: result-folder
public interface Constants {
	String SCREEN_SHOTS_FOLDER = "C:\\Users\\Girmiti\\BBPOS-WP\\bbpos\\screenshots\\";
	String SCREEN_SHOT_FILE_TYPE = "png";
	String READ_CONTACTS="android.permission.READ_CONTACTS";
	String READ_EXTERNAL_STORAGE="android.permission.READ_EXTERNAL_STORAGE";
	String WRITE_EXTERNAL_STORAGE="android.permission.WRITE_EXTERNAL_STORAGE";
	String ACCESS_FINE_LOCATION="android.permission.ACCESS_FINE_LOCATION";
	String ACCESS_COARSE_LOCATION="android.permission.ACCESS_COARSE_LOCATION";
	String BLUE_TOOTH ="android.permission.BLUETOOTH";
	
                               
}
