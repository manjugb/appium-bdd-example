package db.nav.core;

import java.util.List;

import org.apache.log4j.Level;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import db.nav.api.android.Android;
import db.nav.core.manager.DriverManager;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;

public class UIObjectWrapper {

	private final String locator;
	db.nav.core.manager.DriverManager driverManager = new DriverManager();
	AndroidDriver<WebElement> androidDriver;
	private String deviceId = "";
	// private UISelectorWrapper uiLocator;

	public UIObjectWrapper(String locator, String deviceId) {
		this.locator = locator;
		this.deviceId = deviceId;
		setAndroidDroiver();
		MyLogger.logger.setLevel(Level.DEBUG);
	}

	private void setAndroidDroiver() {
		// androidDriver = driverManager.createAndroidDriver();
		// if (Android.driverMap.size() > 0) {
		// androidDriver = Android.driverMap.get(deviceId);
		//
		// }
		// Android.driverMap.
		androidDriver = Android.driverMap.get(this.deviceId);
		// MyLogger.logger.info("LOGGED FROM UIObjectWrapper " + this.deviceId);
		// androidDriver = Android.driver;

	}

	// public void getandroidDriver() {
	// return androidDriver; // = driverManager.createAndroidDriver();
	//
	// }

	private boolean isxPath() {
		return !locator.contains("UiSelector");
	}

	public boolean isVisible() {

		try {
			WebElement element;
			if (isxPath()) {
				element = androidDriver.findElementByXPath(locator);
			} else {
				element = androidDriver.findElementByAndroidUIAutomator(locator);
			}
			return element.isDisplayed();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isElementEmpty() {

		try {
			boolean element;
			if (isxPath()) {
				element = androidDriver.findElementsByXPath(locator).isEmpty();
			} else {
				element = androidDriver.findElementsByAndroidUIAutomator(locator).isEmpty();
			}
			return element;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean isSelected() {
		WebElement element;
		if (isxPath()) {
			element = androidDriver.findElementByXPath(locator);
		} else {
			element = androidDriver.findElementByAndroidUIAutomator(locator);
		}
		return element.isSelected();
	}

	public boolean isChecked() {
		WebElement element;
		element = androidDriver.findElementByXPath(locator);
		if (isxPath()) {
			element = androidDriver.findElementByXPath(locator);
		} else {
			element = androidDriver.findElementByAndroidUIAutomator(locator);
		}
		return element.getAttribute("checked").equals(true);
	}

	public boolean isEnabled() {
		WebElement element;
		element = androidDriver.findElementByXPath(locator);
		return element.getAttribute("enabled").equals(true);
	}

	public boolean isFocused() {
		WebElement element;
		if (isxPath()) {
			element = androidDriver.findElementByXPath(locator);
		} else {
			element = androidDriver.findElementByAndroidUIAutomator(locator);
		}
		return element.getAttribute("focused").equals(true);
	}

	public boolean isClickable() {
		WebElement element;
		if (isxPath()) {
			element = androidDriver.findElementByXPath(locator);
		} else {
			element = androidDriver.findElementByAndroidUIAutomator(locator);
		}
		return element.getAttribute("clickable").equals(true);
	}

	// public String getText(String locator) {
	// if (isxPath()) {
	// return androidDriver.findElementByXPath(locator).getText();
	// } else {
	// return androidDriver.findElementByAndroidUIAutomator(locator).getText();
	// }
	// }

	public String getText() {
		if (isxPath()) {
			return androidDriver.findElementByXPath(locator).getText().toString();
		} else {
			return androidDriver.findElementByAndroidUIAutomator(locator).getText();
		}
	}

	public String getTagName() { // String locator
		if (isxPath()) {
			return androidDriver.findElementByXPath(locator).getTagName();
		} else {
			return androidDriver.findElementByAndroidUIAutomator(locator).getTagName();
		}
	}

	public String getClassName() { // String locator
		if (isxPath()) {
			return androidDriver.findElementByXPath(locator).getAttribute("className");
		} else {
			return androidDriver.findElementByAndroidUIAutomator(locator).getAttribute("className");
		}
	}

	public String getName() {

		if (isxPath()) {
			return androidDriver.findElementByXPath(locator).getAttribute("name");
		} else {
			return androidDriver.findElementByAndroidUIAutomator(locator).getAttribute("name");
		}
	}

	public String contentTDescription() { // String locator
		if (isxPath()) {
			return androidDriver.findElementByXPath(locator).getAttribute("content-Desc");
		} else {
			return androidDriver.findElementByAndroidUIAutomator(locator).getAttribute("content-Desc");
		}
	}

	public Point getLoaction() { // String locator
		if (isxPath()) {
			return androidDriver.findElementByXPath(locator).getLocation();
		} else {
			return androidDriver.findElementByAndroidUIAutomator(locator).getLocation();
		}
	}

	// return a list of specific Webelement i.e.: a list of linear layout object
	public List<WebElement> getElementsList() {
		
		
		List<WebElement>  listElement = androidDriver.findElementsByXPath(locator);
		
		
		if (isxPath()) {
			// MyLogger.logger.info("FINDING ELEMENT BY XPATH:.......... " +
			// locator);
			return androidDriver.findElementsByXPath(locator);
		} else {
			// MyLogger.logger.info("FINDING ELEMENT BY UIAutomator:.......... "
			// + locator);
			return androidDriver.findElementsByAndroidUIAutomator(locator);
		}
	}
	
	
   /* public void click_on_blue_tooth(String wpsname) {
    	
    	List<WebElement> bluetoothlist = new WebDriverWait(androidDriver, 30).until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
		System.out.println("prdoduct=" + bluetoothlist.size());
		for (int i = 0; i < bluetoothlist.size(); i++) {
			String name = bluetoothlist.get(i).getText();
			MyLogger.logger.info("NAME is" + name);

			if (name.equalsIgnoreCase(bluetoothlist)) {
				androidDriver.findElement(By.linkText(bluetoothlist)).click();
			}
		}
	}
    	
    }*/
	
	
	public void click_on_amount() {
		
		
		
	}
	
	
	
	

	public void tap() {
	    /*WebDriverWait wait = new WebDriverWait(androidDriver, 10);
   

		androidDriver.findElementByXPath(locator).click();
		//ait.until(ExpectedConditions.visibilityOfElementLocated(locator)).click();
		try {
			Thread.sleep(500);
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		if (isxPath()) {
			androidDriver.findElementByXPath(locator).click();
		} else {
			androidDriver.findElementById(locator).click();
		}
	}

	public void tapListElement(int element_position) {
		if (!getElementsList().isEmpty() && (getElementsList().size() - 1 > element_position)) {
			getElementsList().get(element_position).click();
		}
	}
	
	
	public void click_key(int key) {
		
	
		///List<MobileElement> elements = androidDriver.findElementsByXPath(locator);
		List<WebElement> entries = androidDriver.findElementsByClassName("android.view.ViewGroup");
		 String  keyelements = entries.get(key).getText();
		 MyLogger.logger.info(keyelements);
		  entries.get(key).click();
	}
			
	

	public void typeText(String text) {
		/*androidDriver.findElementByXPath(locator).sendKeys(text);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		if (isxPath()) {
			androidDriver.findElementByXPath(locator).sendKeys(text);
		} else {
			androidDriver.findElementByAndroidUIAutomator(locator).sendKeys(text);
		}
	}

	public void clearText() {
		if (isxPath()) {
			androidDriver.findElementByXPath(locator).clear();
		} else {
			androidDriver.findElementByAndroidUIAutomator(locator).clear();
		}
	}

	public void scrollToElement() {
		String text = ".";
		try {
			if (!locator.contains("text") && !locator.contains("resourceId")) {
				MyLogger.logger
						.error("locator " + locator + " does not contains text. Element is not identified by text.");
				throw new AssertionError(
						"locator " + locator + " does not contains text. Element is not identified by text.");
			} else if (locator.contains("resourceId")) {
				androidDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()."
						+ "scrollable(true).instance(0)).scrollIntoView( " + locator + ".instance(0));");
			} else {
				if (locator.contains("textcontains")) {
					text = locator.substring(locator.indexOf("[@textcontains=\""), locator.indexOf("\""))
							.replace("[@textcontains=\"", " ");
				} else if (locator.contains("text")) {
					// replacing all special char by whitespace
					// String locatorText = locator.replaceAll("\\W", " ");
					String locatorText = locator.replaceAll("[^a-zA-Z0-9&/-_]", " ");

					text = locatorText.substring(locatorText.indexOf("text"), locatorText.lastIndexOf(" "))
							.replace("text", "").trim();
					// text = text.replace("text", "").trim();
					// String[] textArray = text.split(" ");
					// text = textArray[textArray.length - 2].trim();
					// text = text + " " + textArray[textArray.length -
					// 1].trim();
				}
				androidDriver.findElementByAndroidUIAutomator(
						"new UiScrollable(new UiSelector()." + "scrollable(true).instance(0)).scrollIntoView( new "
								+ "UiSelector().textContains(\"" + text + "\").instance(0));");
				//
				// + ".getChildByText(new
				// UiSelector().className(\"android.widget.TextView\"), " + "\""
				// + text + "\");");
				//
				MyLogger.logger.info("Successffuly scrolled to ............" + text);
			}
		} catch (Exception e) {
			MyLogger.logger.info("scrolling to.." + text + "failed");
			MyLogger.logger.error(e.getMessage() + e.getStackTrace().toString());
			e.printStackTrace();
		}
	}

	public void waitElementToappaer(int seconds) {
		Timer timer = new Timer();
		timer.start();
		while (!timer.isExpired(seconds)) {
			if (isVisible()) {
				 MyLogger.logger.info("element is now displayed" + locator);
				break;
			}
			MyLogger.logger.info("element still not displayed");
		}
		if (timer.isExpired(seconds) && !isVisible()) {
			throw new AssertionError("Element " + locator + " failed to appaer within " + seconds);
		}
	}

	public void waitElementToDisppear(int seconds) {
		Timer timer = new Timer();
		timer.start();
		while (!timer.isExpired(seconds)) {
			if (!isVisible()) {
				break;
			}
		}
		if (timer.isExpired(seconds) && isVisible()) {
			throw new AssertionError("Element " + locator + " failed to disappear within " + seconds);
		}
	}

	public void alertSwitch() throws InterruptedException {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(androidDriver, 5);
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = androidDriver.switchTo().alert();
		alert.accept();

		
	}
	
	public void click_element_javascript() {
		JavascriptExecutor executor = (JavascriptExecutor)androidDriver;
		executor.executeScript("arguments[0].click();", locator);
	}
	
	
	public By clcikByText(String text) {
		return MobileBy.AndroidUIAutomator("new UiSelector().text(\"" + text + "\")");
		
	}

	// public void setFocus() {
	// WebElement element;
	// if (isxPath()) {
	// element = androidDriver.findElementByXPath(locator);
	// } else {
	// element = androidDriver.findElementByAndroidUIAutomator(locator);
	// }
	// return element.getAttribute("focused").value;
	// // return element.getAttribute("focused").equals(true);
	// }

	// public void scrollTo() {
	// // ...
	// }
}
