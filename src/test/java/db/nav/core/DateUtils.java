package db.nav.core;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Random;

/** 
 * 
 * @author Manjunath Golla Bala
 * @{summary} this class defines date utilities
 *
 */

public class DateUtils {

    private static Calendar tmps = Calendar.getInstance();
    private static SimpleDateFormat sdf;

    public static String getMonthStr() {
        sdf = new SimpleDateFormat("M");
        String monthStr = sdf.format(tmps.getTime());
        return monthStr.substring(0, 3);
    }

    public static String getNextMonday() {
        int nexMonday = Calendar.MONDAY;
        String nextMondayStr = "";
		/*int currentDay = 
			if(currentDay != 1)
			{
			 nexMonday = (7-currentDay) + tmps.DAY_OF_MONTH;
			}
			*/
        nextMondayStr = "Monday, " + nexMonday + ". " + getMonthStr() + "Available";
        return nextMondayStr;
    }

    public static String getNextFriday() {
        sdf = new SimpleDateFormat("M");
        return "Friday, " + Calendar.FRIDAY + ". " + getMonthStr() + "Available";
    }

    public static String getRandomDate() {
        Random rand = new Random();
        int today = Calendar.DAY_OF_MONTH;
        int randomDay = rand.nextInt(30 - today);

        //return tmps.DAY_OF_WEEK + "
        //int randomDate = tmps.DAY_OF_MONTH +
        return null;
    }
}
