package db.nav.core;

import java.util.Properties;

public class MultiplePropertyLoader {

    private static Properties properties;

    public static void main(String[] args) {

        try {
            properties = new Properties();
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("C:\\Users\\Girmiti\\BBPOS-WP\\bbpos\\src\\main\\resources\\properties_files\\cst.properties"));
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("resources/properties_files/cst.properties"));
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("resources/properties_files/cst.properties"));

            System.out.println("::: Property File 1 Data :::");
            System.out.println(properties.getProperty("username"));
            System.out.println(properties.getProperty("password"));

            System.out.println("::: Property File 2 Data :::");
            System.out.println(properties.getProperty("username"));
            System.out.println(properties.getProperty("password"));

            System.out.println("::: Property File 3 Data :::");
            System.out.println(properties.getProperty("username"));
            System.out.println(properties.getProperty("password"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}