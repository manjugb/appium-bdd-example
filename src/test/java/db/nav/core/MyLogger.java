package db.nav.core;

import org.apache.log4j.Logger;
/**
 * 
 * @author Manjunath Golla Bala
 * @{summary} this class defines loger, using loger run or debug logs and trace when issues occurs.
 *
 */

public class MyLogger {
	public static Logger logger = Logger.getLogger(MyLogger.class);
}
